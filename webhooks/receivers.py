from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from utils import trigger_model_webhooks


@receiver(post_save, dispatch_uid="instance-saved-hook")
def model_saved(sender, instance, created, raw, using, **kwargs):
    """
    Automatically triggers "created" and "updated" actions.
    """
    action = "created" if created else "updated"
    trigger_model_webhooks(instance, action)


@receiver(post_delete, dispatch_uid="instance-deleted-hook")
def model_deleted(sender, instance, using, **kwargs):
    """
    Automatically triggers "deleted" actions.
    """
    trigger_model_webhooks(instance, "deleted")
