import factory
from factory.django import DjangoModelFactory
from webhooks.models import Action, Webhook, WebhookLog

name = models.CharField(max_length=100)
description = models.CharField(max_length=255, null=True, blank=True)
slug = models.CharField(max_length=50, unique=True)

is_active = models.BooleanField(default=True)


class ActionFactory(DjangoModelFactory):
    
    
    class Meta:
        model = Action


class WebhookFactory(DjangoModelFactory):
    action = factory.SubFactory(ActionFactory)

    class Meta:
        model = Webhook


class WebhookLog(DjangoModelFactory):
    class Meta:
        model = WebhookLog
