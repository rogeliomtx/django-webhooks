from django.db import models
from django.conf import settings
from django.utils import timezone
from django.db.models import F

from swapper import swappable_setting

from webhooks.deliver import Deliver

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class AbstractAction(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, null=True, blank=True)
    slug = models.CharField(max_length=50, unique=True)

    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True
        verbose_name = "Action"
        verbose_name_plural = "Actions"
        swappable = swappable_setting("webhooks", "Action")


class AbsctractWebhook(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="webhooks",
    )

    # definition
    action = models.ForeignKey(
        "Action", on_delete=models.CASCADE, related_name="webhooks"
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, null=True, blank=True)

    # request
    request_headers = models.JSONField(null=True, blank=True)
    request_url_params = models.JSONField(null=True, blank=True)
    callback = models.CharField(max_length=500)

    is_active = models.BooleanField(default=False)

    # zapier
    suscription_id = models.UUIDField(auto_created=True)

    class Meta:
        abstract = True
        verbose_name = "Webhook"
        verbose_name_plural = "Webhooks"
        swappable = swappable_setting("webhooks", "Webhook")

    def deliver(self, instance, action) -> None:
        """
        Delivers the payload to the callback URL.

        Args:
            instance:
            action:

        Returns:

        """
        deliver = Deliver(webhook=self, instance=instance, action=action)
        deliver.deliver()


class AbstractWebhookLog(models.Model):
    class Methods:
        GET = "get"
        OPTIONS = "options"
        HEAD = "head"
        POST = "post"
        PUT = "put"
        PATCH = "patch"
        DELETE = "delete"

        choices = (
            (GET, "GET"),
            (OPTIONS, "OPTIONS"),
            (HEAD, "HEAD"),
            (POST, "POST"),
            (PUT, "PUT"),
            (PATCH, "PATCH"),
            (DELETE, "DELETE"),
        )

    webhook = models.ForeignKey(
        "Webhook", on_delete=models.CASCADE, related_name="logs"
    )
    created_timestamp = models.DateTimeField(auto_now_add=True)

    # retry
    last_retry = models.DateTimeField(null=True)
    last_response = models.TextField(null=True)
    retries = models.PositiveIntegerField(default=0)

    # send
    is_sent = models.BooleanField(default=False)
    sent_timestamp = models.DateTimeField(null=True)

    payload = models.JSONField(null=True, blank=True)

    # instance
    instance_content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    instance_object_id = models.CharField(
        max_length=255, blank=True, null=True
    )
    instance = GenericForeignKey("instance_content_type", "instance_object_id")

    # callbacks
    method = models.CharField(
        max_length=10, choices=Methods.choices, default=Methods.POST
    )

    class Meta:
        abstract = True
        verbose_name = "Webhook log"
        verbose_name_plural = "Webhook logs"
        swappable = swappable_setting("webhooks", "WebhookLog")

    def mark_as_sent(self):
        self.is_sent = True
        self.sent_timestamp = timezone.now()
        self.save()

    def update_last_try(self, response):
        self.last_retry = timezone.now()
        self.last_response = response.text
        self.retries += 1
        self.save()
