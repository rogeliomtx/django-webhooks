from django.dispatch import Signal

"""
Model hook triggered

Args:
    action:
    instance:
"""
webhook_triggered = Signal()

"""
Custom hook triggered

Args:
    event_name:
    payload:
    user:
"""
custom_webhook_triggered = Signal()

"""
Webhook sent

Args:
    payload:
    instance:
    hook:
"""
webhook_sent = Signal()


"""
Webhook received
"""
webhook_received = Signal()
