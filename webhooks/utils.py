from swapper import load_model

Webhook = load_model("webhooks", "Webhook")


def get_model_label(instance) -> str or None:
    """

    Args:
        instance: Any object

    Returns:
        "app_label.model" for example: "users.User"
    """
    if instance is None:
        return None
    opts = instance._meta.concrete_model._meta
    try:
        return opts.label
    except AttributeError:
        return ".".join([opts.app_label, opts.object_name])


def trigger_model_webhooks(instance, action):
    # check that the event is allwed

    # expected result: app.Model.action
    model_label = get_model_label(instance)
    action = f"{model_label}.{action}"

    # get all hooks to be sent
    webhooks = Webhook.objects.filter(
        action__slug=action,
        **instance.webhook_filters(action),
    )

    for webhook in webhooks:
        webhook.deliver(instance)


def get_module(path):
    """
    A modified duplicate from Django's built in backend retriever.
        slugify = get_module('django.template.defaultfilters.slugify')
    """
    from importlib import import_module

    try:
        mod_name, func_name = path.rsplit(".", 1)
        mod = import_module(mod_name)
    except ImportError as e:
        raise ImportError(f'Error importing alert function {mod_name}: "{e}"')

    try:
        func = getattr(mod, func_name)
    except AttributeError:
        raise ImportError(
            f'Module "{mod_name}" does not define a "{func_name}" function'
        )

    return func
