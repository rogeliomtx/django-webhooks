from swapper import load_model

WebhookLog = load_model("webhooks", "WebhookLog")


class Deliver:
    def __init__(self, webhook, payload, instance, action):
        self.webhook = webhook
        self.instance = instance
        self.action = action
        self.payload = payload

    def deliver(self):
        pass

    def deliver_async(self):
        from webhooks.tasks import DeliverTask

        log = WebhookLog.objects.create(
            webhook=self.webhook,
            instance=self.instance,
            payload=self.payload,
        )

        DeliverTask.delay(log.id, self.payload, self.instance.id, self.action)
