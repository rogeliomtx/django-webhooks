from django.core import serializers

from webhooks.base.models import (
    AbstractAction,
    AbsctractWebhook,
    AbstractWebhookLog,
)


class WebhookModel:
    def webhook_serialize(self, action, fields=None, **kwargs):
        """

        Args:
            action (str): model action are "created", "updated" and "deleted",
                custom actions are supported too.
            fields (list of str): List of fields to be sent in the webhook
                payolad.
            **kwargs: Any other configuration por the serializer

        Returns:
            a JSON serializer data
        """
        if fields:
            return serializers.serialize(
                "json", [self], fields=fields, **kwargs
            )
        else:
            return serializers.serialize("json", [self], **kwargs)

    def webhook_user(self):
        """
        Gets the user related to the instance. This is used to filter the
            webhook delivery.
        """
        return self.user

    def webhook_filters(self, action):
        """

        Args:
            action (str): model action are "created", "updated" and "deleted",
                custom actions are supported too.

        Returns:
            (dict): 'queryset.filter' arguments
        """
        return {"user": self.webhook_user()}


class Action(AbstractAction):
    class Meta(AbstractAction.Meta):
        abstract = False


class Webhook(AbsctractWebhook):
    class Meta(AbsctractWebhook.Meta):
        abstract = False


class WebhookLog(AbstractWebhookLog):
    class Meta(AbstractWebhookLog.Meta):
        abstract = False
