from django.conf import settings

import requests
from swapper import load_model
from celery.app.task import Task
from webhooks.exceptions import WebhookNotSent

webhook_settings = settings.WEBHOOKS
WebhookLog = load_model("webhooks", "WebhookLog")


class DeliverTask(Task):
    max_retries = webhook_settings.get("max_retries", 3)
    default_retry_delay = webhook_settings.get("default_retry_delay", 3 * 60)

    def run(self, log_id, payload, instance_id, action):
        deliver_webhook(log_id, payload, instance_id, action, task=self)


def deliver_webhook(log_id, payload, instance_id, action, task=None):
    log = WebhookLog.objects.get(id=log_id)
    webhook = log.webhook

    headers = {"Content-Type": "application/json"}
    if webhook.request_headers:
        headers.update(**webhook.request_headers)

    params = {}
    if webhook.request_url_params:
        params.update(webhook.request_url_params)

    response = requests.request(
        log.method,
        url=webhook.callback,
        data=payload,
        headers=headers,
        params=params,
    )

    # create a log
    if 200 >= response.status_code < 300:
        log.mark_as_sent()
    elif 300 >= response.status_code < 600:
        # 300's 400's and 500's means it failed
        log.update_last_try(response)
    else:
        log.update_last_try(response)
        if task:
            task.retry(exc=WebhookNotSent(
                f"response.status_code: {response.status_code}."
            ))
